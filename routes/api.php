<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\contactController;
use App\Http\Controllers\TshirtSizeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Api routes
Route::post('/login', [AuthController::class, 'login']); //login
Route::post('/register', [AuthController::class, 'register']); //register

//Routes accessible only to administrator
Route::middleware(['auth:sanctum', 'abilities:administrator'])->group(function () {
    Route::get('/contacts/export', [contactController::class, 'exportToExcel']); //export to excel csv
    Route::delete('/contacts/{id}', [contactController::class, 'deleteContact']); //delete contact

   
    Route::post('/size/add', [TshirtSizeController::class, 'addSize']); //add a tshirt size
    Route::delete('/size/{id}', [TshirtSizeController::class, 'deleteSize']); //delete tshirt size
    Route::put('/size/{id}', [TshirtSizeController::class, 'updateSize']); //update tshirt size
});

//Routes accessible to both administrator and data capturer
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/sizes', [TshirtSizeController::class, 'getAllSizes']); //get all tshirt sizes
    Route::post('/search', [contactController::class, 'search']); //search
    Route::post('/logout', [AuthController::class, 'logout']); //logout
    Route::get('/contacts', [contactController::class, 'getAllContacts']); //get all contacts
    Route::post('/contacts/add', [contactController::class, 'addContact']); //add a contact
    Route::put('/contacts/{id}', [contactController::class, 'updateContact']); //update a contact
});
