<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\TshirtSize;
use Exception;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function getAllContacts()
    {
        try {
            $contacts = Contact::all();
            return response(['message' => 'successfully retrieved all records', 'data' => $contacts], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while fetching the records",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function addContact(Request $request)
    {
        try {
            $size = TshirtSize::where('size', $request->t_shirt)->first(); //verifying if t shirt size is available

            if ($size) {
                $data = $request->validate([
                    'name' => 'required|string|min:2',
                    'surname' => 'required|string|min:2',
                    'mobile_number' => 'required|string|min:10|max:10|unique:contacts,mobile_number',
                    'address' => 'required|string',
                    't_shirt' => 'required|string|min:1|max:2'
                ]);

                $contact = new Contact;
                $contact->name =  $data['name'];
                $contact->surname = $data['surname'];
                $contact->mobile_number = $data['mobile_number'];
                $contact->address = $data['address'];
                 $contact->t_shirt = $data['t_shirt'];
                $contact->save();

                return response(['message' => 'successfully added a record'], 200);
} else {
                return response(['message' => 'The t shirt size is not available'], 404);
            }
           
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while adding the record",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function updateContact($id, Request $request)
    {
        try {
            $size = TshirtSize::where('size', $request->t_shirt)->first(); //verifying if t shirt size is available

            if ($size) {
                $data = $request->validate([
                    'name' => 'required|string|min:2',
                    'surname' => 'required|string|min:2',
                    'mobile_number' => 'required|string|min:10|max:10',
                    'address' => 'required|string',
                    't_shirt' => 'required|string|min:1|max:2'
                ]);
                
                $contact = Contact::find($id);
                $contact->name =  $data['name'];
                $contact->surname = $data['surname'];
                $contact->mobile_number = $data['mobile_number'];
                $contact->address = $data['address'];
                $contact->t_shirt = $data['t_shirt'];
                $contact->save();

                return response(['message' => 'successfully updated record'], 200);
            } else {
                return response(['message' => 'The t shirt size is not available'], 404);
            }
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while updating the record",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function deleteContact($id, Request $request)
    {
        try {
            $contact = Contact::find($id);
            $contact->delete();
            return response(['message' => 'successfully deleted a record'], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while deleting the record",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function getContact($id, Request $request)
    {
        try {
            $contact = Contact::where($id)->get();
            return response(['message' => 'successfully retrieved record'], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while fetching the record",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function search( Request $request)
    {
        try {
           $query=$request->get('query');
          $results=Contact::where("name","like","%".$query."%")->orWhere("surname","like","%".$query."%")->
          orWhere("mobile_number","like","%".$query."%")->get();
            return response(['message' => 'successfully retrieved record','data'=>$results], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while fetching the records",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function exportToExcel( Request $request){
        try{
            $file="";
           return response(['message' => 'successfully exported to excel file','file'=>$file], 200);
        }catch(Exception $err){
            return response(
                [
                    'message' => "Something went wrong while exporting to excel file",
                    'error' => $err
                ],
                404
            );
        }
        return "works";
    }
}
