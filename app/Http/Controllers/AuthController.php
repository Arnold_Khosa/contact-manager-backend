<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $password = $request->password;
            $user = User::where('email', $request->email)->first();
           
            if (Hash::check($password, $user->password)) {

                $token = $user->createToken($user['firstname'].' '.$user['lastname'], [$user['role']])->plainTextToken;
                $response = [
                    'message' => 'Success',
                    'token' => $token,
                    'data' => $user,
                ];
                return response($response, 200);
            } else {
                throw new Exception("Login failed");
            }
        } catch (Exception $error) {
            return response(['error' => $error, 'message' => 'Login failed'], 403);
        }
    }
    public function register(Request $request)
    {
        $data = $request->validate([
            'firstname' => 'required|string|min:2',
            'lastname' => 'required|string|min:2',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:1',
        ]);
        $user = User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
        $response = [
            'message' => 'successfully registered',
            'data' => $user
        ];
        return response($response, 200);
    }
    public function logout(Request $request)
    {
    try{
        $bearerToken = $request->bearerToken();
        //retrieve the access token from Personal access token table;
        $token = \Laravel\Sanctum\PersonalAccessToken::findToken($bearerToken);
        //retrieve user id from users table
        $userId = $token->tokenable_id;
//delete tokens of curremt user
$user = \Laravel\Sanctum\PersonalAccessToken::where('tokenable_id',$userId)->delete();

return response(['message'=>'logged out'],200);
    }catch(Exception $err){
        $response = [
            'message' => 'Something went wrong ',
            'error' => $err
        ];
        return response($response, 403);
    }
    }
}
