<?php

namespace App\Http\Controllers;

use App\Models\TshirtSize;
use Exception;
use Illuminate\Http\Request;

class TshirtSizeController extends Controller
{
    public function getAllSizes()
    {
        try {
            $sizes = TshirtSize::all();
            return response(['message' => 'successfully retrieved all records', 'data' => $sizes], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while fetching the records",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function addSize(Request $request)
    {
        try {
            $data = $request->validate([
                'size' => 'required|string|min:1|max:2'
            ]);
            $size = new TshirtSize;
            $size->size = strtoupper($data['size']);
            $size->save();
            return response(['message' => 'successfully added a record'], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while adding the record",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function updateSize($id, Request $request)
    {
        try {
            $data = $request->validate([
                'size' => 'required|string|min:1|max:2'
            ]);

            $size = TshirtSize::find($id);
            $size->size =  strtoupper($data['size']);
            $size->save();
            return response(['message' => 'successfully updated record'], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while updating the records",
                    'error' => $err
                ],
                404
            );
        }
    }
    public function deleteSize($id, Request $request)
    {
        try {
            $size = TshirtSize::find($id);
            $size->delete();
            return response(['message' => 'successfully deleted a record'], 200);
        } catch (Exception $err) {
            return response(
                [
                    'message' => "Something went wrong while deleting the record",
                    'error' => $err
                ],
                404
            );
        }
    }
}
