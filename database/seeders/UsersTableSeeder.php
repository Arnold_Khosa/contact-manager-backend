<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
             ['firstname'=>'John',
            'lastname'=>'Doe',
             'email'=>'john@gmail.com',
             'password'=>'$2y$10$OmdBTtU0Hihjb8bwSVBJEO3oCeofwNfqCdYHwp2.sam51zvI4HGRu',
            'role'=>'datacapturer'
        ],
        [
            'firstname'=>'Arnold',
            'lastname'=>'khosa',
             'email'=>'arnold@gmail.com',
             'password'=>'$2y$10$OmdBTtU0Hihjb8bwSVBJEO3oCeofwNfqCdYHwp2.sam51zvI4HGRu',
            'role'=>'administrator'
        ]
        ]);
    }
}